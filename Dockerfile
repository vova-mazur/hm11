FROM node:10

COPY public ./public
COPY *.html ./
COPY passport.config.js ./
COPY server.js ./
COPY package*.json ./

RUN npm install

CMD ["node", "server.js"]
EXPOSE 8000